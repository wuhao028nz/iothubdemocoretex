package com.coretex.iothubdemocoretex

import android.content.Context
import android.util.Log
import com.microsoft.azure.sdk.iot.device.*
import com.microsoft.azure.sdk.iot.device.DeviceTwin.Property
import com.microsoft.azure.sdk.iot.device.DeviceTwin.TwinPropertyCallBack
import com.microsoft.azure.sdk.iot.device.transport.IotHubConnectionStatus

val TAG = "IoTHubCallbacks"

class IotHubConnectionStatusChangeCallbackLogger : IotHubConnectionStatusChangeCallback {
    override fun execute(
        status: IotHubConnectionStatus,
        statusChangeReason: IotHubConnectionStatusChangeReason,
        throwable: Throwable?,
        callbackContext: Any
    ) {
        Log.d(TAG, "CONNECTION STATUS UPDATE: $status")
        Log.d(TAG, "CONNECTION STATUS REASON: $statusChangeReason")
        Log.d(
            TAG,
            "CONNECTION STATUS THROWABLE: " + if (throwable == null) "null" else throwable.message
        )

        throwable?.printStackTrace()

        when (status) {
            IotHubConnectionStatus.DISCONNECTED -> {
                // connection was lost, and is not being re-established. Look at provided exception for
                // how to resolve this issue. Cannot send messages until this issue is resolved, and you manually
                // re-open the device client

                callbackContext.takeIf { callbackContext is Context }?.let {
                    (callbackContext as Context).apply {

                    }
                }
            }
            IotHubConnectionStatus.DISCONNECTED_RETRYING -> {
                // connection was lost, but is being re-established. Can still send messages, but they won't
                // be sent until the connection is re-established
            }
            IotHubConnectionStatus.CONNECTED -> {
                callbackContext.takeIf { callbackContext is Context }?.let {
                    (callbackContext as Context).apply {

                    }
                }
                // Connection was successfully re-established. Can send messages.
            }
        }
    }
}

class CorehubIotMessageCallback : com.microsoft.azure.sdk.iot.device.MessageCallback {
    override fun execute(msg: Message, context: Any): IotHubMessageResult {
        Log.d(
            TAG,
            "Received message with content: " + String(
                msg.bytes,
                Message.DEFAULT_IOTHUB_MESSAGE_CHARSET
            )
        )
        return IotHubMessageResult.COMPLETE
    }
}

class DeviceTwinStatusCallBack : IotHubEventCallback {
    override fun execute(status: IotHubStatusCode, context: Any) {
        if (status === IotHubStatusCode.OK || status === IotHubStatusCode.OK_EMPTY) {
            MainActivity.Succeed.set(true)
        } else {
            MainActivity.Succeed.set(false)
        }
        Log.d(TAG, "IoT Hub responded to device twin operation with status " + status)
    }
}

class onProperty : TwinPropertyCallBack {
    override fun TwinPropertyCallBack(property: Property, context: Any) {
        Log.d(
            TAG,
            "onProperty callback for " + (if (property.getIsReported()) "reported" else "desired") +
                    " property " + property.key +
                    " to " + property.value +
                    ", Properties version:" + property.version
        )
    }
}
package com.coretex.iothubdemocoretex

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.microsoft.azure.sdk.iot.device.DeviceClient
import com.microsoft.azure.sdk.iot.device.IotHubClientProtocol
import com.microsoft.azure.sdk.iot.device.IotHubStatusCode
import com.microsoft.azure.sdk.iot.device.Message
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.math.pow

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val protocol = IotHubClientProtocol.MQTT
        val client = DeviceClient(BuildConfig.DeviceConnectionString, protocol)
        client.registerConnectionStatusChangeCallback(
            IotHubConnectionStatusChangeCallbackLogger(),
            application
        )
        var counter = 1
        client.apply {
            try {
                this.closeNow()
                open()
                val callback = CorehubIotMessageCallback()
                setMessageCallback(callback, application)
                Succeed.set(false)
                startDeviceTwin(
                    DeviceTwinStatusCallBack(), application,
                    onProperty(), application
                )

                do {
                    Thread.sleep(3.toDouble().pow(counter.toDouble()).toLong() * 1000L)
                    counter++
                } while (!Succeed.get() && counter <= 3)
            } catch (ex: IOException) {
                client.closeNow()
                Log.e(TAG, ex.message)
            }
        }

        start_button.setOnClickListener {
            client.sendEventAsync(
                Message("coretex message"),
                { responseStatus: IotHubStatusCode, callbackContext: Any ->
                    Log.d(TAG, "coretex message send $responseStatus")
                },
                this@MainActivity
            )
        }

    }


    companion object {
        val Succeed = AtomicBoolean(false)
    }

}
